<?php
/*
Plugin Name: Jeff Mikels Websites
Plugin URI: none
Description: This plugin helps administer the payments for Jeff Mikels Websites
Author: Jeff Mikels
Text Domain: jeff-mikels-websites
Version: 0.2
Author URI: http://jeff.mikels.cc
*/

// WORDPRESS MODIFICATION FUNCTIONS
function jmw_install()
{
	$role = get_role( 'administrator' );
	$role->add_cap('jmw_admin');

	if (! wp_next_scheduled ( 'jmw_cron' )) {
		wp_schedule_event(time(), 'daily', 'jmw_cron');
	}
	
}
function jmw_uninstall()
{
	$role = get_role( 'administrator' );
	$role->remove_cap('jmw_admin');

	wp_clear_scheduled_hook('jmw_cron');
}
register_activation_hook(__FILE__, 'jmw_install');
register_deactivation_hook(__FILE__, 'jmw_uninstall');

function jmw_lipsum($atts)
{
	$content = Array();
	$content[] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a congue neque. Vivamus lobortis lobortis elit dapibus consectetur. Etiam venenatis accumsan sem sed maximus. Proin quam metus, viverra nec velit in, eleifend porttitor ipsum. Nulla sit amet augue in augue vehicula gravida id vel nisl. Quisque elementum, ex eget consequat auctor, diam sapien efficitur libero, sed lacinia lacus diam eget metus. Curabitur laoreet, est vel vestibulum sodales, sapien dolor accumsan risus, at finibus risus libero eu mi. Vestibulum augue ipsum, congue ut lacinia vitae, varius eu libero. Pellentesque quis felis sed erat dignissim dignissim. Phasellus sed ultrices libero. Sed ac leo placerat, lacinia lorem id, ultricies mi. Nullam sit amet consectetur tellus. Etiam in sollicitudin libero. Proin porttitor felis eu leo fringilla porta. Suspendisse a massa tellus.';
	$content[] = 'Nullam blandit molestie nulla ut dignissim. Vivamus eleifend tortor odio, ut ornare elit convallis tempor. Integer tempus convallis sem, at pulvinar libero pulvinar sed. Aenean vitae iaculis lectus. In eget elementum ipsum, ac iaculis mauris. Phasellus sed volutpat neque, id feugiat nunc. Praesent ultrices, enim ut mollis semper, felis arcu lacinia orci, eget ultrices metus neque dictum nulla. In mattis condimentum metus, id maximus nibh faucibus a. Duis mi enim, consectetur vel auctor nec, dapibus nec velit. Duis eleifend eleifend feugiat. Curabitur consectetur justo ut ex auctor, et interdum nibh tempus. Quisque id convallis urna. Praesent imperdiet in dolor nec finibus. Integer in cursus dolor. Proin a est tellus. Nullam vel nulla vel erat pharetra commodo.';
	$content[] = 'Phasellus lobortis non tortor vel scelerisque. Maecenas sagittis lectus a volutpat viverra. Nunc posuere rhoncus rhoncus. Suspendisse potenti. Duis a ante a neque viverra blandit. Proin iaculis dignissim porta. Morbi volutpat commodo diam, a dapibus erat semper ut.';
	$content[] = 'Mauris posuere risus velit. Quisque luctus ex lorem, ut aliquam tortor semper non. Donec eget facilisis est. Maecenas vitae dui sed ipsum pharetra bibendum. Phasellus facilisis pharetra dictum. Ut in nulla porttitor, ullamcorper felis vitae, luctus metus. Praesent accumsan lobortis lorem, accumsan lobortis lacus porttitor ac. Nulla ac ligula bibendum, lobortis diam eu, ullamcorper lectus. Donec a consequat ipsum, eu interdum arcu. Integer vel pretium dui. Mauris nec justo id orci hendrerit finibus.';
	$content[] = 'Suspendisse quis arcu accumsan, pharetra sapien vel, viverra purus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed sodales, eros non vulputate feugiat, ipsum urna sollicitudin elit, non venenatis nunc mi quis tortor. Nam a diam porta, pretium nisl eget, ultricies massa. Ut in elementum enim. Mauris a nulla pharetra, condimentum nibh eget, ultrices ex. Mauris non congue risus. Integer tincidunt tellus sit amet nisi mollis, vel malesuada sapien vulputate. Fusce tristique, nulla ac accumsan eleifend, nisl magna sagittis justo, a faucibus odio urna vitae mi. Nulla non porta tellus, vitae tristique mauris. Proin ac dolor justo.';
	
	return '<p>' . implode('</p><p>', $content) . '</p>';
}
add_shortcode('lipsum', 'jmw_lipsum');

function jmw_bipsum($attrs)
{
	return <<<EOF
<p>Jesus told his disciples: <span class="wj">“There was a rich man whose manager was accused of wasting his possessions.</span> <span class="wj">So he called him in and asked him, ‘What is this I hear about you? Give an account of your management, because you cannot be manager any longer.’</span> </p>

<p><span class="wj">“The manager said to himself, ‘What shall I do now? My master is taking away my job. I’m not strong enough to dig, and I’m ashamed to beg—I know what I’ll do so that, when I lose my job here, people will welcome me into their houses.’</span> </p>

<p><span class="wj">“So he called in each one of his master’s debtors. He asked the first, ‘How much do you owe my master?’</span> </p>

<p><span class="wj">“ ‘Nine hundred gallons of olive oil,’ he replied. </span></p>

<p><span class="wj">“The manager told him, ‘Take your bill, sit down quickly, and make it four hundred and fifty.’</span> </p>

<p><span class="wj">“Then he asked the second, ‘And how much do you owe?’ </span></p>

<p><span class="wj">“ ‘A thousand bushels of wheat,’ he replied. </span></p>

<p><span class="wj">“He told him, ‘Take your bill and make it eight hundred.’</span> </p>

<p><span class="wj">“The master commended the dishonest manager because he had acted shrewdly. For the people of this world are more shrewd in dealing with their own kind than are the people of the light.</span> <span class="wj">I tell you, use worldly wealth to gain friends for yourselves, so that when it is gone, you will be welcomed into eternal dwellings.</span> </p>

<p><span class="wj">“Whoever can be trusted with very little can also be trusted with much, and whoever is dishonest with very little will also be dishonest with much.</span> <span class="wj">So if you have not been trustworthy in handling worldly wealth, who will trust you with true riches?</span> <span class="wj">And if you have not been trustworthy with someone else’s property, who will give you property of your own?</span> </p>

<p><span class="wj">“No one can serve two masters. Either you will hate the one and love the other, or you will be devoted to the one and despise the other. You cannot serve both God and money.”</span> </p>

<p>The Pharisees, who loved money, heard all this and were sneering at Jesus. He said to them, <span class="wj">“You are the ones who justify yourselves in the eyes of others, but God knows your hearts. What people value highly is detestable in God’s sight.</span> </p>

<p><span class="wj">“The Law and the Prophets were proclaimed until John. Since that time, the good news of the kingdom of God is being preached, and everyone is forcing their way into it.</span> <span class="wj">It is easier for heaven and earth to disappear than for the least stroke of a pen to drop out of the Law.</span> </p>

<p><span class="wj">“Anyone who divorces his wife and marries another woman commits adultery, and the man who marries a divorced woman commits adultery.</span> </p>

<p><span class="wj">“There was a rich man who was dressed in purple and fine linen and lived in luxury every day.</span> <span class="wj">At his gate was laid a beggar named Lazarus, covered with sores</span> <span class="wj">and longing to eat what fell from the rich man’s table. Even the dogs came and licked his sores.</span> </p>

<p><span class="wj">“The time came when the beggar died and the angels carried him to Abraham’s side. The rich man also died and was buried.</span> <span class="wj">In Hades, where he was in torment, he looked up and saw Abraham far away, with Lazarus by his side.</span> <span class="wj">So he called to him, ‘Father Abraham, have pity on me and send Lazarus to dip the tip of his finger in water and cool my tongue, because I am in agony in this fire.’</span> </p>

<p><span class="wj">“But Abraham replied, ‘Son, remember that in your lifetime you received your good things, while Lazarus received bad things, but now he is comforted here and you are in agony.</span> <span class="wj">And besides all this, between us and you a great chasm has been set in place, so that those who want to go from here to you cannot, nor can anyone cross over from there to us.’</span> </p>

<p><span class="wj">“He answered, ‘Then I beg you, father, send Lazarus to my family,</span> <span class="wj">for I have five brothers. Let him warn them, so that they will not also come to this place of torment.’</span> </p>

<p><span class="wj">“Abraham replied, ‘They have Moses and the Prophets; let them listen to them.’</span> </p>

<p><span class="wj">“ ‘No, father Abraham,’ he said, ‘but if someone from the dead goes to them, they will repent.’</span> </p>

<p><span class="wj">“He said to him, ‘If they do not listen to Moses and the Prophets, they will not be convinced even if someone rises from the dead.’ ”</span></p>
<p class="bipsum_ref">Luke 16 NIV</p>
EOF;
}
add_shortcode('bipsum', 'jmw_bipsum');

function jmw_add_styles()
{
	?>

	<style>
	span.wj {color:#b00;}
	.bipsum_ref {text-align:right;font-style:italic;font-weight:bold;}
	.jmw-box {line-height:1.1em;text-align:center;padding:16px;font-weight:bold;margin:0;box-sizing:border-box;width:100%;font-size:8pt;color:black;background:rgba(255,180,180,.95);position:fixed;bottom:0;left:0;z-index:99;}
	.jmw-box p {padding:0;margin:0;}
	.jmw-error {border-color:red;}

	.jmw-related-pages li {padding-left:5px;}
	.jmw-related-pages .current-menu-item {background:rgba(128,128,128,.1);}
	.jmw-related-pages li.parent {}
	.jmw-related-pages li.sibling {padding-left:15px;}
	.jmw-related-pages li.child {padding-left:25px;}
	.jmw-error a {color:black;font-weight:bold;display:inline-block;padding:6px;background-color:red;border:1px solid black;border-radius:3px;}
	.jmw-error a:hover {background:black;color:red;}
	
	.jmw-form-pair {margin:10px;}
	.jmw-form-pair label {font-size:2em;text-transform:uppercase;font-weight:bold;}
	.jmw-form-pair input {width:60%;font-size:2em;margin-top:10px;}

/*	.jmw-subscription-form table {width:100%;text-align:left;}*/
/*	.jmw-subscription-form table input[type="text"] {width: 60%;}*/
	.jmw-subscription-form select {font-size:30px;height:50px;margin:20px;}
	</style>

	<?php
}
add_action('wp_head', 'jmw_add_styles');
add_action('admin_head', 'jmw_add_styles');


/* Add Administration Pages */
if ( is_admin() )
{
	add_action ('admin_menu', 'jmw_admin_menu');
	add_action ('admin_init', 'jmw_register_options');
}

function jmw_admin_menu()
{
	// admins can see the whole options page allowing for the override of the nag bar
	add_options_page('Jeff Mikels Websites', 'Jeff Mikels Websites', 'publish_posts', 'jmw-options', 'jmw_options_page');
	
	// managers can see the payment page only
	// add_submenu_page(null, 'Jeff Mikels Websites', 'Jeff Mikels Websites', 'publish_posts', 'jmw-payment', 'jmw_payment_page');

	// add_submenu_page(null, 'Jeff Mikels Websites', 'Jeff Mikels Websites', 'publish_posts', 'jmw-payment-cancel', 'jmw_payment_cancel_page');
}

function jmw_setup_stripe() {
	// by doing this here, we never put the Stripe key into a variable that some other
	// plugin might be able to discover
	require_once('stripe/init.php');
	require_once('conf-private.php');
}

// function jmw_payment_page()
// {
// 	jmw_handle_post();
// 	jmw_show_stripe_charge_form();
// }

function jmw_show_stripe_charge_form() {
	jmw_setup_stripe();
	
	// get possible products, match to subscription plans, and render dropdown
	$products = \Stripe\Product::all();
	$plans = \Stripe\Plan::all();
	$products_by_id = [];
	$selections = [];
	foreach ($products->data as $product)
	{
		if (!$product->active) continue;
		$products_by_id[$product->id] = $product;
	}
	foreach($plans->data as $key => $plan)
	{
		if (!$plan->active) continue;
		$product_id = $plan->product;
		if (!empty($products_by_id[$product_id]))
		{
			$product = $products_by_id[$product_id];
			$plan->product_data = $product;
			$selections[$product->id] = ['name' => $product->name, 'plan' => $plan->id, 'amount' => $plan->amount];
		}
	}
	uasort($selections, function($a, $b) {if ($a['amount'] == $b['amount']) return 0; return ($a['amount'] < $b['amount']) ? -1 : 1;})
	
	?>
	
	<!-- stripe elements form -->
	<style>
		.StripeElement {
		  box-sizing: border-box;

		  height: 40px;

		  padding: 10px 12px;

		  border: 1px solid transparent;
		  border-radius: 4px;
		  background-color: white;

		  box-shadow: 0 1px 3px 0 #e6ebf1;
		  -webkit-transition: box-shadow 150ms ease;
		  transition: box-shadow 150ms ease;
		}

		.StripeElement--focus {
		  box-shadow: 0 1px 3px 0 #cfd7df;
		}

		.StripeElement--invalid {
		  border-color: #fa755a;
		}

		.StripeElement--webkit-autofill {
		  background-color: #fefde5 !important;
		}
		
		.jmw-subscription-form {margin-bottom:100px;}
		
		.jmw-subscription-form label {
		  font-family: "Helvetica Neue", Helvetica, sans-serif;
		  font-size: 16px;
		  font-variant: normal;
		  padding: 0;
		  margin: 0;
		  -webkit-font-smoothing: antialiased;
		}

		.jmw-subscription-form button {
		  border: none;
		  border-radius: 4px;
		  outline: none;
		  text-decoration: none;
		  color: #fff;
		  background: #32325d;
		  white-space: nowrap;
		  display: inline-block;
		  height: 40px;
		  line-height: 40px;
		  padding: 0 14px;
		  box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
		  border-radius: 4px;
		  font-size: 15px;
		  font-weight: 600;
		  letter-spacing: 0.025em;
		  text-decoration: none;
		  -webkit-transition: all 150ms ease;
		  transition: all 150ms ease;
/*		  float: left;*/
		  margin-left: 12px;
		  margin-top: 28px;
		}

		.jmw-subscription-form button:hover {
		  transform: translateY(-1px);
		  box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
		  background-color: #43458b;
		}

		.jmw-subscription-form form {
/*		  padding: 30px;*/
/*		  height: 120px;*/
		}

		.jmw-subscription-form label {
		  font-weight: 500;
		  font-size: 14px;
		  display: block;
		  margin-bottom: 8px;
		}

		.jmw-subscription-form #card-errors {
		  height: 20px;
		  padding: 4px 0;
		  color: #fa755a;
		}

		.jmw-subscription-form .form-row {
/*		  width: 70%;*/
/*		  float: left;*/
		}

		.jmw-subscription-form .token {
		  color: #32325d;
		  font-family: 'Source Code Pro', monospace;
		  font-weight: 500;
		}

		.jmw-subscription-form .wrapper {
		  width: 670px;
		  margin: 0 auto;
		  height: 100%;
		}

		.jmw-subscription-form #stripe-token-handler {
		  position: absolute;
		  top: 0;
		  left: 25%;
		  right: 25%;
		  padding: 20px 30px;
		  border-radius: 0 0 4px 4px;
		  box-sizing: border-box;
		  box-shadow: 0 50px 100px rgba(50, 50, 93, 0.1),
		    0 15px 35px rgba(50, 50, 93, 0.15),
		    0 5px 15px rgba(0, 0, 0, 0.1);
		  -webkit-transition: all 500ms ease-in-out;
		  transition: all 500ms ease-in-out;
		  transform: translateY(0);
		  opacity: 1;
		  background-color: white;
		}

		.jmw-subscription-form #stripe-token-handler.is-hidden {
		  opacity: 0;
		  transform: translateY(-80px);
		}

		.jmw-subscription-form input {height:40px;border-radius:4px;width:100%;margin-bottom:0;font-size:18px;padding:0 10px;}
/*		.jmw-subscription-form #payment-form {width:70%;}*/
		.jmw-subscription-form .form-row {margin-bottom:20px;}
		.jmw-subscription-form .errors {color:red;font-weight:bold;}
	</style>
	<form action="" method="post" id="payment-form">
		<div class="form-row">
			<select name="plan-id" id="product" onchange="" size="1">
				<option value="">-- SELECT SUBSCRIPTION --</option>
		
				<?php foreach($selections as $prod) : ?>
			
				<option value="<?php print $prod['plan']; ?>">
					<?php print $prod['name']; ?> ($<?php print number_format($prod['amount'] / 100, 0) ;?> / mo.)
				</option>
		
				<?php endforeach;?>
			</select>
			<div id="product-errors" class="errors" role="alert"></div>
			
		</div>
		
		<div class="form-row">
			<label for="name-element">
				Your Name (required)
			</label>
			<div id="jmw-name-element">
				<input type="text" name="jmw-name" id="jmw-name"/>
			</div>
			<div id="jmw-name-errors" class="errors" role="alert"></div>
		</div>

		<div class="form-row">
			<label for="email-element">
				Billing Email (required)
			</label>
			<div id="jmw-email-element">
				<input type="text" name="jmw-email" id="jmw-email"/>
			</div>
			<div id="jmw-email-errors" class="errors" role="alert"></div>
		</div>

		<div class="form-row">
			<label for="card-element">
				Credit or debit card
			</label>
			
			<div id="card-element">
				<!-- A Stripe Element will be inserted here. -->
			</div>

			<!-- Used to display form errors. -->
			<div id="card-errors" role="alert"></div>
		</div>
		
		<div class="form-row">
			<button id="submit-button">Submit Payment</button>
		</div>
	</form>
	
	<!-- include stripe javascript code -->
	<script src="https://js.stripe.com/v3/"></script>
	<script>
		let stripe = Stripe('pk_live_Y6l6EeunB1zZqVEkCQp7GfTx');

		// Create an instance of Elements.
		var elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base: {
				color: '#32325d',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		};

		// Create an instance of the card Element.
		var card = elements.create('card', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
		card.mount('#card-element');

		// Handle real-time validation errors from the card Element.
		card.addEventListener('change', function(event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
		});

		// Handle form submission.
		var form = document.getElementById('payment-form');
		form.addEventListener('submit', function(event) {
			event.preventDefault();
			
			let name = document.getElementById('jmw-name').value;
			if (name == '') document.getElementById('jmw-name-errors').textContent = 'Please enter your name';
			else document.getElementById('jmw-name-errors').textContent = '';

			let email = document.getElementById('jmw-email').value;
			if (!email.match(/.+@.+\..+/)) document.getElementById('jmw-email-errors').textContent = 'This field must be a valid email address';
			else document.getElementById('jmw-email-errors').textContent = '';
			

			let plan = document.getElementById('product').value;
			if (plan == '') document.getElementById('product-errors').textContent = 'You must select a subscription';
			else document.getElementById('product-errors').textContent = '';

			if (name == '' || plan == '' || !email.match(/.+@.+\..+/)) return;
			
			let el = document.getElementById('submit-button');
			el.setAttribute('disabled', 'disabled');
			el.innerHTML = 'Submitting...';
			
			extraData = {
				name: document.getElementById('jmw-name').value,
			}
			stripe.createToken(card, extraData).then((result) => {
				if (result.error) {
					el.removeAttribute('disabled');
					el.innerHTML = 'Submit Payment';
				
					// Inform the user if there was an error.
					var errorElement = document.getElementById('card-errors');
					errorElement.textContent = result.error.message;
				} else {
					console.log('got token: ' + result.token);
					// Send the token to your server.
					stripeTokenHandler(result.token);
				}
			});
		});

		// Submit the form with the token ID.
		function stripeTokenHandler(token) {
			// Insert the token ID into the form so it gets submitted to the server
			var form = document.getElementById('payment-form');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'stripeToken');
			hiddenInput.setAttribute('value', token.id);
			form.appendChild(hiddenInput);

			// Submit the real form
			form.submit();
		}
	</script>
		
	<?php
}

function jmw_cron()
{
	jmw_check_subscription();
}
add_action('jmw_cron', 'jmw_cron');

function jmw_check_subscription()
{
	jmw_setup_stripe();
	$stored_options = get_option('jmw_options');
	$customer_id = $stored_options['stripe_customer'];
	$subscription_id = $stored_options['stripe_subscription'];
	if ($customer_id != '' && $subscription_id != '')
	{
		// TODO check to see if stripe subscription is active
		$subscription = \Stripe\Subscription::retrieve($subscription_id);
		$ended = $subscription->ended_at;
		$active = $subscription->status == 'active';
		if ($active)
		{
			$stored_options['subscription_active'] = 1;
		}
		else
		{
			$stored_options['subscription_active'] = 0;
			// $stored_options['stripe_subscription'] = '';
		}
		update_option('jmw_options', $stored_options, false);
		wp_cache_delete('jmw_options', 'options');
		return $subscription;
	}
	return ($stored_options['subscription_active'] == 1);
}

function jmw_options_page()
{
	if ( !current_user_can('publish_posts') ) wp_die( __('You do not have sufficient permissions to access this page.' ) );
	
	if (isset($_GET['settings-updated']) && $_GET['settings-updated']) flush_rewrite_rules();
	jmw_setup_stripe();
	
	// handle posted form data if it exists
	if (!empty($_POST['stripeToken']))
	{
		// we have a token
		$token = $_POST['stripeToken'];
		$email = $_POST['jmw-email'];
		$name = $_POST['jmw-name'];
		$plan_id = $_POST['plan-id'];
		$blogname = get_bloginfo('name');
		// create the customer and payment source
		$customer = \Stripe\Customer::create([
			"description" => "'$name' <$email> from $blogname",
			"email" => $email,
			"name" => $name,
			"source" => $token // obtained with Stripe.js
		]);
			
		// subscribe the customer to the chosen product and plan
		$subscription = \Stripe\Subscription::create([
			"customer" => $customer->id,
			"items" => [
				[
					"plan" => $plan_id,
				],
			]
		]);
		
		// save data to Wordpress
		$new_data = get_option('jmw_options');
		$new_data['subscription_active'] = 1;
		$new_data['stripe_customer'] = $customer->id;
		$new_data['stripe_subscription'] = $subscription->id;
		update_option('jmw_options', $new_data, false);
		wp_cache_delete('jmw_options', 'options');
	}
	else
	{
		$new_data = [];

	}
	
	$subscription = jmw_check_subscription();
	
	$options = Array(
		'coupon_code'=>Array(
			'type'=>'text',
			'label'=>'Coupon Code',
			'value'=>'',
			'description'=>'If you have been issued a coupon code, enter it here and hit the save changes button.',
			'admin_only' => 0
		),
		// 'last_payment'=>Array(
		// 	'type'=>'text',
		// 	'label'=>'Last Payment Date',
		// 	'value'=>'',
		// 	'description'=>'Your last payment date.',
		// 	'admin_only' => 1
		// ),
		'hide_bar'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Hide the nag bar',
			'value'=>0,
			'description'=>'',
			'admin_only' => 1
		),
		'subscription_active'=>Array(
			'type'=>'checkbox',
			'checkvalue'=>1,
			'label'=>'Subscription is Active',
			'value'=>0,
			'description'=>'',
			'admin_only' => 1
		),
		'stripe_customer'=>Array(
			'type'=>'text',
			'label'=>'Stripe Customer ID',
			'value'=>'',
			'description'=>'Stripe Customer ID',
			'admin_only' => 1
		),
		'stripe_subscription'=>Array(
			'type'=>'text',
			'label'=>'Stripe Subscription ID',
			'value'=>'',
			'description'=>'Stripe Subscription ID',
			'admin_only' => 1
		),
	);


	// populate values from those stored in the database
	$stored_options = get_option('jmw_options');
	foreach ($stored_options as $key=>$value)
	{
		if (!empty($options[$key]) && !empty($value))
			$options[$key]['value'] = $value;
	}
	
	// override with results of posted data if it exists
	foreach ($new_data as $key=>$value)
	{
		if (!empty($options[$key]) && !empty($value))
			$options[$key]['value'] = $value;
	}
	
	// override subscription defaults
	if ($options['coupon_code']['value'] == 'jeremiah2911') $options['subscription_active']['value'] = 1;
	
	
	?>

	<div class="wrap">
		<h2>Jeff Mikels' Websites Payment Page</h2>

		<?php if ($subscription === TRUE || ($subscription->created && $subscription->ended_at === NULL)) : ?>

		<div class="jmw_thanks updated">
			<p>
				Thank you for subscribing to Jeff Mikels' Hosting by <a href="http://jeff.mikels.cc">Jeff Mikels.</a>
				It is truly my hope and prayer that this site allows you to proclaim the Gospel of Jesus more effectively.
			</p>
			
			<?php if ($subscription !== TRUE) : ?>

				<p>
					Your Subscription started on <strong><?php print date('m/d/Y', $subscription->created);?></strong> and is set to renew on <strong><?php print date('m/d/Y', $subscription->current_period_end); ?></strong>.
				</p>
				
			<?php endif;?>

		</div>

		<?php endif; ?>
		<?php if ((!$options['subscription_active']['value']) || (current_user_can('jmw_admin'))): ?>


		<form method="post" action="options.php">
			<?php settings_fields('jmw_options'); ?>

			<table class="form-table">
				<?php foreach ($options as $key=>$value): ?>

					<?php if (($value['admin_only'] == 1) && (! current_user_can('jmw_admin'))) continue; ?>

					<tr valign="top">
						<th scope="row"><?php echo $value['label']; ?><?php if ($value['admin_only'] == 1):?><br />JMW ADMIN ONLY<?php endif; ?></th>
						<td>
							<?php if ($value['type'] == 'checkbox') : ?>
							<input name="jmw_options[<?php echo $key; ?>]" type="checkbox" value="<?php echo htmlentities($value['checkvalue']); ?>" <?php checked('1', $value['value']); ?> /> <?php echo $value['label']; ?>
							<?php elseif ($value['type'] == 'text') : ?>
							<input style="width:60%;" name="jmw_options[<?php echo $key; ?>]" type="text" value="<?php echo htmlentities($value['value']); ?>" />
							<?php elseif ($value['type'] == 'disabled') : ?>
							<input style="width:60%;"
								name="jmw_options[<?php echo $key; ?>]"
								type="text"
								disabled="disabled"
								value="<?php echo htmlentities($value['value']); ?>"
								/>
							<?php else: ?>
							<input
								style="width:60%;"
								name="jmw_options[<?php echo $key; ?>]"
								type="<?php echo $value['type']; ?>"
								value="<?php echo htmlentities($value['value']); ?>"
								/>
							<?php endif; ?>
							<p><?php echo $value['description']; ?></p>
						</td>
					</tr>

				<?php endforeach; ?>

			</table>

			<?php submit_button(); ?>

		</form>

		<?php if (! $options['subscription_active']['value']) : ?>

			<div class="jmw-subscription-form" style="text-align:center;padding:20px;background-color:#bbb;border:6px solid #999;border-radius:6px;">
				<h2>Start Your Subscription</h2>

				<?php jmw_show_stripe_charge_form(); ?>

			</div>
		<?php endif;?>

	<?php endif;?>
	</div>

	<?php
}


function jmw_payment_confirm_page()
{
	if (isset($_POST['payment_status']) && $_POST['payment_status'] == 'Completed')
	{
		$stored_options = get_option('jmw_options');
		// $stored_options['paypal_active'] = 1;
		$stored_options['subscription_active'] = 1;
		update_option('jmw_options', $stored_options);
		?>

		<div class="updated">
			<p>Your first payment has been processed. Thank you!</p>
		</div>

		<?php
	}
	else
	{
		?>

		<div class="error">
			<p>Your payment did not go through. Perhaps you should try again.</p>
		</div>

		<?php

	}

	?>

	<a href="<?php echo admin_url('options-general.php?page=jmw-options'); ?>">Go back to options page.</a>

	<?php

}

function jmw_register_options()
{
	register_setting('jmw_options','jmw_options'); // one group to store all options as an array
}

add_action('init', 'jmw_payment_check');
function jmw_payment_check()
{
	if (isset($_GET['jmw-payment-confirm']))
	{
		jmw_payment_confirm_page();
		exit();
	}
}

// PUT SUBSCRIPTION NAG BAR IN THE FOOTER OF ADMIN PAGES
add_action('admin_footer', 'jmw_subscription_check');
function jmw_subscription_check()
{
	// check for subscription
	$stored_options = get_option('jmw_options',array());
	if ($stored_options['hide_bar'] == 1) return;
	if (! empty($stored_options['subscription_active']) && $stored_options['subscription_active'] == 1) return;

	$new_html = '<div class="jmw-box jmw-error">';
	$new_html .= '<p>This website is hosted by Jeff Mikels Websites, but the subscription is currently inactive.';

	if (current_user_can('publish_posts'))
	{
		$new_html .= '<p>Because you are a manager of this site, you can activate the subscription now by clicking here: <a href="' . admin_url('options-general.php?page=jmw-options') . '">SUBSCRIBE</a>.';
	}
	$new_html .= '</div>';
	print $new_html;
	// $content = $new_html . $content;
	// return $content;
}

// Add related pages sidebar widget
class JMW_RelatedPagesWidget extends WP_Widget
{
	function __construct()
	{
		$widget_ops = array('classname' => 'JMW_RelatedPagesWidget', 'description' => 'Adds a widget to automatically generate a "related pages" list showing parent pages and child pages of the current page.');
		parent::__construct('JMW_RelatedPagesWidget', 'Related Pages List', $widget_ops);
	}

	function widget($args, $instance)
	{
		// only show widget on Pages
		global $post;
		if ($post->post_type != 'page') return;

		// we want to generate a list of parent, siblings, and children
		$parent = NULL;
		$children = jmw_my_get_child_pages($post->ID);

		// if there is a parent page, generate the page menu from there
		if ($post->post_parent)
		{
			$parent = get_post($post->post_parent);
			$siblings = jmw_my_get_child_pages($post->post_parent);
		}
		else
		{
			$siblings = array($post);
		}

		// if there are no children, and no parent ignore this widget
		if (empty($children) && empty($parent)) return;

		echo $args['before_widget'];

		?>

		<div class="widget-wrap">
			<div class="jmw-related-pages">
				<h4 class="widgettitle widget-title">Related Pages</h4>
				<ul>
					<?php if (!empty($parent)): ?>
						<!-- PARENT PAGE -->
						<li class="parent">
							<a href="<?php print get_permalink($parent->ID); ?>"><?php print $parent->post_title; ?></a>
						</li>
					<?php endif;?>

					<ul>

						<?php foreach ($siblings as $sibling) : ?>

							<li class="sibling <?php if ($sibling->ID == $post->ID) print "current-menu-item"; ?>">
								<a href="<?php print get_permalink($sibling->ID); ?>"><?php print $sibling->post_title; ?></a>
							</li>

							<?php if ($sibling->ID == $post->ID && !empty($children)): ?>

								<ul>

									<?php foreach($children as $child): ?>
										<li class="child">
											<a href="<?php print get_permalink($child->ID); ?>"><?php print $child->post_title; ?></a>
										</li>
									<?php endforeach;?>

								</ul>

							<?php endif;?>

						<?php endforeach; ?>

					</ul>
				</ul>
			</div>
		</div>

		<?php

		echo $args['after_widget'];
	}
}
add_action( 'widgets_init', function() {return register_widget("JMW_RelatedPagesWidget"); });

// HELPER FUNCTIONS
function jmw_my_get_child_pages($id, $orderby = 'menu_order') {
	$children = get_children( array (
		'post_type' => 'page',
		'post_status' => 'publish',
		'post_parent' => $id,
		'orderby' => $orderby,
		'order' => 'ASC'
	) );
	return $children;
}


?>
